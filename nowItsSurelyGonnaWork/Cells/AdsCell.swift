//
//  AdsCell.swift
//  ios_market
//
//  Created by Dell Smith on 20/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class AdsCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var oldCost: UILabel!
    @IBOutlet weak var newCost: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    
    func loadData(ads: AdsPrototype) {
        imgView.image = ads.img
        if (ads.oldCost == nil) { oldCost.text = ""}
        else {
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: ads.oldCost!)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            oldCost.attributedText = attributeString
        }
        if (ads.newCost == nil) { newCost.text = ""}
        else { newCost.text = ads.newCost! }
        if (ads.desc == nil)  {desc.text = ""}
        else {desc.text = ads.desc!}
    }
    
}
