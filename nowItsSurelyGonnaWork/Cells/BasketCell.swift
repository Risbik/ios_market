//
//  BasketCell.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 01/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

protocol ReloadData {
    func reloadData()
    func updateItem(index: Int, newNumber: UInt32)
}

class BasketCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var costView: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var numberView: UITextField!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    var _delegate: ReloadData!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        

    }
    
    @IBAction func addNum(_ sender: Any) {
        var num = Int(numberView.text ?? "0")
        num! += 1
        numberView.text = String(num!)
        //if (_delegate == nil) { }
        _delegate.updateItem(index: self.tag, newNumber: UInt32(num!))
        _delegate.reloadData()
    }
    
    @IBAction func subNum(_ sender: Any) {
        var num = Int(numberView.text ?? "0")
        num! -= 1
        if (num! <= 0) {
            BasketViewModel.removeItem(_item: BasketViewModel.getItemsInBasket()[self.tag].item)
            _delegate.reloadData()
            return
        }
        
        numberView.text = String(num!)
         _delegate.updateItem(index: self.tag, newNumber: UInt32(num!))
        _delegate.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
