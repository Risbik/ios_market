//
//  detailedViewCell.swift
//  ios_market
//
//  Created by Dell Smith on 21/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import Foundation
import UIKit

class ImgViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
}

class TextViewCell: UITableViewCell {
    @IBOutlet weak var textView: UILabel!
}

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
}
