//
//  customCell.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 22/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class DataCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var costView: UILabel!
    
    var _data: Item!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(_ data: Item) {
        _data = data
    }
    @IBAction func buyPressed(_ sender: Any) {
        BasketViewModel.addToBasket(_item: _data, _number: 1)
    }
}
