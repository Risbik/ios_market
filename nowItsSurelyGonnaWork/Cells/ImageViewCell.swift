//
//  ImageViewCell.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 02/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit

class ImageViewCell: UICollectionViewCell {
    var imgView: UIImageView
    
    override init(frame: CGRect) {
        
        
        
        imgView = UIImageView()
        
        super.init(frame: frame)
        
        backgroundColor = .green
        
        addSubview(imgView)
        
        imgView.translatesAutoresizingMaskIntoConstraints = false
        
        imgView.backgroundColor = .white
        
        imgView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        imgView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        imgView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imgView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        imgView.contentMode = .scaleAspectFit

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
