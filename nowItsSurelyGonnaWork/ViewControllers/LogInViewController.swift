//
//  LogInController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 06/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class LogInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var welcomeText: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var signInText: UILabel!
    @IBOutlet weak var errorMsg: UILabel!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var newUserText: UILabel!
    var users: UserDataModel?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        users = JSONParser.decodeUserData()
        viewDidLoadAnim()
        

        
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        userName.inputAccessoryView = toolbar
        userPassword.inputAccessoryView = toolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @IBAction func logIn(_ sender: Any) {
        
        logIn()
    
    }
    
    @IBAction func goToRegister(_ sender: Any) {
        let registerViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "registerView")
        
        //registerViewController.users = users
        
        navigationController?.pushViewController(registerViewController, animated: true)
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case userName : userPassword.becomeFirstResponder()
        default:
            logIn()
        }
        
        return true
    }

    func logIn() {
        let user_name = userName.text!
        let user_password = userPassword.text!
        var error = true
        if (users != nil) {
            for user in users!.users
            {
                if user_name == user.userName
                {
                    if user_password == user.userData.userPassword
                    {
                        let marketView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBar") as! UITabBarController
                        User.currentUser = user
                        error = false
                        marketView.navigationItem.hidesBackButton = true
                        marketView.view.contentMode = .scaleAspectFit
                        
                        navigationController?.pushViewController(marketView, animated: true)
                        break
                    }
                    
                }
                
            }
        }
        
        //userName.text = "No such user"
        //userPassword.text = "or incorrect password"
        if (!error) {
            return
        }
        let color = logInButton.backgroundColor
        let frame = logInButton.frame
        let leftPos = logInButton.frame.offsetBy(dx: -15, dy: 0)
        let rightPos = logInButton.frame.offsetBy(dx: 15, dy: 0)
        
        UIView.animate(withDuration: 1, animations: {
            self.logInButton.backgroundColor = UIColor.red
            self.logInButton.backgroundColor = color
            self.logInButton.frame.origin.x = leftPos.origin.x
            self.logInButton.frame.origin.x = rightPos.origin.x
            self.logInButton.frame.origin.x = frame.origin.x
            self.errorMsg.alpha = 1
        })
        
    }
    
    
    func viewDidLoadAnim() {
        UIView.animate(withDuration: 0.4
            , animations: {
                self.welcomeAppier()
        }) { (true) in
            UIView.animate(withDuration: 0.2, animations: {
                self.signInAppier()
            }, completion: { (true) in
                UIView.animate(withDuration: 0.2,
                               animations: {
                                self.textFieldsAppier()
                }, completion: { (true) in
                    UIView.animate(withDuration: 0.2
                        , animations: {
                            self.registerAppier()
                            }, completion: nil)
                })
            })
        }
    }
    
    func welcomeAppier() {
        let welcomeFrame = welcomeText.frame
        welcomeText.frame = welcomeText.frame.insetBy(dx: -100, dy: 0)
        self.welcomeText.frame = welcomeFrame
        
    }
    
    func signInAppier() {
        let signInFrame = signInText.frame
        signInText.frame = signInText.frame.insetBy(dx: -100, dy: 0)
        
                self.signInText.frame = signInFrame
        
    }
    
    func textFieldsAppier() {
       
            self.userName.alpha = 1
            self.userPassword.alpha = 1
            self.logInButton.alpha = 1
        
    }
    
    func registerAppier() {
        
            self.newUserText.alpha = 1
            self.registerButton.alpha = 1
        
    }
    
}
