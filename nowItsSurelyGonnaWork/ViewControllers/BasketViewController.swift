//
//  BacketViewController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 28/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation




class BasketViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ReloadData {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    //static var inBasket = [Items]()
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var soldView: UILabel!
    @IBOutlet weak var costView: UILabel!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
        updateTotalCost()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateTotalCost()
        tableView.reloadData()
        
    }
    
    func updateTotalCost() {
        
        let items = BasketViewModel.getItemsInBasket()
        var costSum: UInt32 = 0
        for item in BasketViewModel.getItemsInBasket() {
            costSum += (item.item.cost * item.number)
        }
        
        buyButton.setTitleColor((items.count == 0) ? #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1) : #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1), for: .normal)
        
        buyButton.isEnabled = !(items.count == 0)
        costView.text = (items.count == 0) ? "0" : String(costSum)
        self.tabBarController?.tabBar.items![2].badgeValue = costView.text
        backgroundView.isHidden = !(items.count == 0)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return BasketViewModel.getItemsInBasket().count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basketCell", for: indexPath) as! BasketCell
        
        let data = BasketViewModel.getItemsInBasket()[indexPath.section]
        weak var _item = data.item
        
       
        cell.nameView.text = _item!.name
        cell.costView.text = String(_item!.cost)
        cell.imgView.image = _item!.img[0]
        cell.deleteButton.tag = indexPath.section
        cell.tag = indexPath.section
        cell.deleteButton.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        cell.cellView.layer.cornerRadius = 10
        cell.cellView.layer.masksToBounds = true
        cell.numberView.text = String(data.number)
        cell._delegate = self
        
        return cell
    }
    // MARK - TODO
    @IBAction func buttonPressed(sender: UIButton!)
    {
        BasketViewModel.removeItem(_item: BasketViewModel.getItemsInBasket()[sender.tag].item)
        tableView.reloadData()
        updateTotalCost()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func buyPressed(_ sender: Any) {
        BasketViewModel.emptyBasket()
        UIView.animate(withDuration: 2.0, animations:
            {
                self.soldView.alpha = 1
                self.soldView.alpha = 0
        })
        tableView.reloadData()
        updateTotalCost() 
        
    }
    
    func reloadData() {
        tableView.reloadData()
        updateTotalCost()
    }
    
    func updateItem(index: Int, newNumber: UInt32) {
        BasketViewModel.updateItem(index: index, newNumber: newNumber)
    }
    
    
    
//    func addDoneButtonOnKeyboard()
//    {
//        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
//        doneToolbar.barStyle = .default
//
//        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
//
//        let items = [flexSpace, done]
//        doneToolbar.items = items
//        doneToolbar.sizeToFit()
//
//        self.inputAccessoryView = doneToolbar
//    }
//
//    @objc func doneButtonAction()
//    {
//        self.resignFirstResponder()
//    }
    
    @IBAction func goToMarket(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    
    
}



//extension UITextField{
//
//    @IBInspectable var doneAccessory: Bool{
//        get{
//            return self.doneAccessory
//        }
//        set (hasDone) {
//            if hasDone{
//                addDoneButtonOnKeyboard()
//            }
//        }
//    }


