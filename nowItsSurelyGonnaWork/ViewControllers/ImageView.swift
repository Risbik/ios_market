//
//  ImageView.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 02/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit

class ImageView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   

    var images: [UIImage?] = []
    
    init(_images: [UIImage?]) {
        images = _images
        
        let collection = UICollectionViewFlowLayout()
        collection.scrollDirection = .horizontal
        super.init(frame: .zero, collectionViewLayout: collection)
        
        backgroundColor = .white
        
        dataSource = self
        delegate = self
        
        translatesAutoresizingMaskIntoConstraints = false
        
        register(ImageViewCell.self, forCellWithReuseIdentifier: "imgCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let scrSize = self.window?.frame
        let width = 300
        let height = 300
        return CGSize(width: width, height: height)
        
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! ImageViewCell
        
        cell.imgView.image = images[indexPath.row]
        
        return cell
    }
    
}
