//
//  UserProfileViewController.swift
//  ios_market
//
//  Created by Vladislav on 11/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    weak var user: UserKey!
    @IBOutlet weak var userNameView: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        user = User.currentUser
        userNameView.text = user.userName
    }
    

    @IBAction func saveChanges(_ sender: Any) {
        let userData = UserData()
        
        let newUser = UserKey()
        newUser.userName = user.userName
        newUser.userData = userData
        
        JSONParser.updateUserData(user: newUser)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Password"
        case 1:
            return "First name"
        default:
            return "Last name"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileViewCell
        
        switch indexPath.section {
        case 0:
            cell.textField.text = user.userData.userPassword
            cell.textField.isSecureTextEntry = true
        case 1:
            cell.textField.text = user.userData.userFirstName
        default:
            cell.textField.text = user.userData.userLastName
        }
        
        return cell
    }
    
}
