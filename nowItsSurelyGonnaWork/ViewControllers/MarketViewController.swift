//
//  MainController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 27/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit





class MarketViewController: UIViewController, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    
    var ads: [AdsPrototype] = []
    var data: [Item] = []
    
    var menuShown = false
    
    @IBOutlet weak var itemView: UITableView!
    @IBOutlet weak var adsView: UICollectionView!
    @IBOutlet weak var sortButton: UIBarButtonItem!
    @IBOutlet weak var itemViewContainer: UIView!
    let numberOfItemsPerRow = 3
    
    var itemViewModel: ItemViewModel? = nil
    var menuViewModel: MenuViewModel? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adsView.tag = 1
        sortButton.action = #selector(showSortAllert(_:))
        loadData()
        loadAds()
        
        itemViewModel = ItemViewModel()
        menuViewModel = MenuViewModel()
        
        itemViewModel?.data = data       
        
        
        
        
        print("hello")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateBasketBadge()
        //navigationController?.navigationBar.isHidden = true
    }
    

    
    func itemButton(_ title: String?, _ image: UIImage) -> UIButton {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        button.setTitle(title, for: .normal)
        button.setImage(image, for: .normal)
        button.setTitleColor(UIColor.white, for: .selected)
        button.applyNavBarConstraints()
        
        return button
    }
    
    func loadAds() {
        var temp = AdsPrototype()
        temp.img = UIImage(named: "pepsi_ads")
        temp.oldCost = "25"
        temp.newCost = "20"
        temp.desc = "SELL"
        ads.append(temp)
        
        temp = AdsPrototype()
        temp.img = UIImage(named: "pepsi_limited")
        temp.oldCost = "Single pepsi cost 25"
        temp.newCost = "Total cost 110"
        temp.desc = "Buy MORE pay LESS"
        ads.append(temp)
    }
    
    func loadData() {
        var _imgs: [UIImage?] = []
        //cola set up
        _imgs = [UIImage(named: "cola"),
                 UIImage(named: "cola2"),
                 UIImage(named: "cola3"),
                 UIImage(named: "cola4")]
        var temp: Item = Item("cola",
                              30,
                              "It's a cola. Kinda good drink from USA (but I'm not sure). You should try it out,It's a cola. Kinda good drink from USA (but I'm not sure). You should try it out,It's a cola. Kinda good drink from USA (but I'm not sure). You should try it out",
                              _imgs)
        data.append(temp)
        //pepsi set up
        _imgs = [UIImage(named: "pepsi"),
                 UIImage(named: "pepsi2"),
                 UIImage(named: "pepsi3"),
                 UIImage(named: "pepsi4")]
        temp = Item("pepsi",
                    25,
                    "Not a cola, but pepsi. Steel good drink, actualy, like cola, but have different name",
                    _imgs)
        data.append(temp)
        //7up set up
        _imgs = [UIImage(named: "7up"),
                 UIImage(named: "7up2"),
                 UIImage(named: "7up3"),
                 UIImage(named: "7up4")]
        temp = Item("7up",
                    27,
                    "Is anybody know what is that? No? Me too. What does '7' mean, what is 'up', dunno anything abot this",
                    _imgs)
        data.append(temp)
    }
    
    @IBAction func moreButtonTapped(_ sender: UIButton) {
        let _data = data[sender.tag]
        
        let popUpInfo = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "info") as! DetailedViewController
                
        popUpInfo.data = _data

        
        self.navigationController?.pushViewController(popUpInfo, animated: true)
    }


    
    
    
    

    
    
    @IBAction func showHideMenu(_ sender: Any) {
        if (menuShown) {
            UIView.animate(withDuration: 0.8, animations: {
                self.itemViewContainer.frame.origin.x = 60
            })
        }
        else {
            UIView.animate(withDuration: 0.8, animations: {
            self.itemViewContainer.frame.origin.x = self.view.frame.width - 140
            })
            
        }
        menuShown = !menuShown
    }

    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView.tag == 1) {return ads.count}
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "adsCell", for: indexPath) as! AdsCell
            cell.loadData(ads: ads[indexPath.row])
            print("ads cell loaded")
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if (collectionView.tag != 1) {
        let popUpInfo = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "info") as! DetailedViewController
        popUpInfo.data = data[indexPath.row]
        navigationController?.pushViewController(popUpInfo, animated: true)
        }
    }
    
    

    
    func updateBasketBadge() {
        let items = BasketViewModel.getItemsInBasket()
        var cost: UInt32 = 0
        
        for item in items {
            cost += item.item.cost * item.number
        }
        
        self.tabBarController?.tabBar.items![2].badgeValue = String(cost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (collectionView.tag != 1) {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
            return CGSize(width: size, height: size)
        } else {
            var size = collectionView.frame.size
            size.height = size.height - 10
            return size
        }
    }
    
    @objc func showSortAllert(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: "Sort items via..", preferredStyle: .actionSheet)
        
        let defaultAction = UIAlertAction(title: "name", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.sortByName()
        })
        
        let deleteAction = UIAlertAction(title: "price", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.sortByPrice()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            //
        })
        
        alertController.addAction(defaultAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sortByName() {
        data =  itemViewModel!.sortByName()
        itemView.reloadData()
    }
    
    func sortByPrice() {
        data = itemViewModel!.sortByPrice()
        itemView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! DataCell
        let _data = data[indexPath.row]
        cell.nameView.text = _data.name
        cell.costView.text = String(_data.cost)
        cell.imgView.image = _data.img[0]
        cell.layer.masksToBounds = true
//        cell.layer.borderColor = UIColor.darkGray.cgColor
//        cell.layer.borderWidth = 0.8
        cell._data = _data
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let popUpInfo = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "info") as! DetailedViewController
        popUpInfo.data = data[indexPath.row]
        navigationController?.pushViewController(popUpInfo, animated: true)
    }
}

extension UIView {
    func applyNavBarConstraints() {
        let widthConstraint = self.widthAnchor.constraint(equalToConstant: 25)
        let heightConstraint = self.heightAnchor.constraint(equalToConstant: 25)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
    }
}
