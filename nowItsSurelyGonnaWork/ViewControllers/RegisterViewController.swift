//
//  RegisterViewController.swift
//  ios_market
//
//  Created by Vladislav on 10/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class RegisterViewController: UIViewController, UITextFieldDelegate {

    var users: UserDataModel?
    @IBOutlet weak var lastNameView: UITextField!
    @IBOutlet weak var firstNameView: UITextField!
    @IBOutlet weak var passwordView: UITextField!
    @IBOutlet weak var loginView: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var userNameError: UILabel!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var firstNameError: UILabel!
    @IBOutlet weak var lastNameError: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        loginView.inputAccessoryView = toolbar
        passwordView.inputAccessoryView = toolbar
        firstNameView.inputAccessoryView = toolbar
        lastNameView.inputAccessoryView = toolbar
    }
 
    
    @IBAction func registerNewUser(_ sender: Any) {
        
        register()
        
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    func register() {
        hideAllErrors()
        
        var check = true
        let user_name = loginView.text!
        let user_password = passwordView.text!
        let firstName = firstNameView.text!
        let lastName = lastNameView.text!
        
        if user_name == "" {
            check = false
            errorAnim()
            showError(label: userNameError)
        }
        
        if user_password == "" {
            //passwordView.text = "Password can't be empty!"
            check = false
            errorAnim()
            showError(label: passwordError)
            
            
        }
        
        if (firstName == "") {
            showError(label: firstNameError)
            check = false
        }
        
        if (lastName == "") {
            showError(label: lastNameError)
            check = false
        }
        if users != nil {
            for user in users!.users
            {
                if user_name == user.userName
                {
                    showError(label: userNameError)
                    //loginView.text = "This login is taken by some else"
                    check = false
                    errorAnim()
                    
                    
                }
            }
        }
        else { users = UserDataModel() }
        if (!check) {
            return
        }
        
        let newUser = UserKey()
        let newUserData = UserData()
        
        newUserData.userPassword = user_password
        newUserData.userLastName = lastName
        newUserData.userFirstName = firstName
        newUserData.userMoney = 0
        
        newUser.userName = user_name
        newUser.userData = newUserData
        
        users!.users.append(newUser)
        
        JSONParser.saveUserData(users: users!)
        
        User.currentUser = newUser
        
        let marketView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBar") as! UITabBarController
        
        marketView.navigationItem.hidesBackButton = true
        marketView.view.contentMode = .scaleAspectFit
        
        navigationController?.pushViewController(marketView, animated: true)
    }
    
    func errorAnim()
    {
        let color = registerButton.backgroundColor
        
        UIView.animate(withDuration: 1, animations: {
            self.registerButton.backgroundColor = UIColor.red
            self.registerButton.backgroundColor = color
            
        })
        
    }
    
    func showError(label: UILabel) {
        UIView.animate(withDuration: 0.5, animations: {label.alpha = 1 })
    }
    /*
    func hideError(label: UILabel) {
        UIView.animate(withDuration: 0.5, animations: {label.isHidden = true })
    }*/

    func hideAllErrors() {
        userNameError.alpha = 0
        passwordError.alpha = 0
        firstNameError.alpha = 0
        lastNameError.alpha = 0
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case loginView:
            passwordView.becomeFirstResponder()
        case passwordView:
            firstNameView.becomeFirstResponder()
        case firstNameView:
            lastNameView.becomeFirstResponder()
        default:
            register()
        }
         return true
    }
}
