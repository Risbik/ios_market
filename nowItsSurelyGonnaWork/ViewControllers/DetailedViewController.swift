//
//  popUpInfoController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 22/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class DetailedViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout,
UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var itemCost: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var basket: UIImageView!
    @IBOutlet weak var number: UITextField!
    @IBOutlet weak var like: UIImageView!
    @IBOutlet weak var notificationViewConstraint: NSLayoutConstraint!
    
    
    var inProgress = true
    var prevPos: CGRect!
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var num: Int = 0
    
    
    weak var data: Item!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        
        
        
       Update()
    }
    
    func Update() {
        
        number.text = "1"
        
        buyButton.layer.cornerRadius = 15
        buyButton.layer.masksToBounds = true
        
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        number.inputAccessoryView = toolbar
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @IBAction func addToBasket(_ sender: Any) {
        
        num = Int(number.text ?? "1")!
        if (num == 0) {
            return
        }
        BasketViewModel.addToBasket(_item: data, _number: UInt32(num))
        itemCost.text = String(data.cost * UInt32(num))
        if (num > 1) {
        itemName.text = String(num) + "x" + data.name
        } else {itemName.text = data.name}
        num = 1
        number.text = "1"
        
        UIView.animate(withDuration: 1, animations: {
            self.like.alpha = 1
            self.like.alpha = 0
            })
        
        self.notificationView.isHidden = false
        

        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionCrossDissolve, animations: {
            self.notificationView.alpha = 1
        }, completion: { finished in
            UIView.animate(withDuration: 1.7, animations: {
                self.notificationView.alpha = 0
            })
        })
        
        
    }

    
    @IBAction func add(_ sender: Any) {
        num = num + 1
        number.text = String(num)
    }
    @IBAction func sub(_ sender: Any) {
        num = num - 1
        if (num < 1) {num = 1}
        number.text = String(num)
    }
    
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return ""
        case 1:
            return "Product info"
        default:
            return "Description"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case 0: do {
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgViewCell", for: indexPath) as! ImgViewCell
                return cell
            }
        case 1: do {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textViewCell", for: indexPath) as! TextViewCell
            cell.textView.text = "Name: " + String(data.name) + "\n" + "Cost: " + String(data.cost)
            return cell
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "textViewCell", for: indexPath) as! TextViewCell
            cell.textView.text = data.desc
            return cell
        }
        


    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: do {
            return 270
            }
        case 1: do {
            return 50
            }
        default:
            return UITableView.automaticDimension
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.img.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! CollectionViewCell
        cell.imgView.image = data.img[indexPath.row]
        print("cell loaded")
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return size
    }
    

}
