//
//  MenuViewModel.swift
//  ios_market
//
//  Created by Гость on 18/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import Foundation
import UIKit

class MenuViewModel {
    @IBOutlet var tableView: UITableView!
    
    init() {
        
    }
    
    
    func getCell(indexPath: IndexPath) -> UITableViewCell {
            let cell = MenuCell()
            let menuModel = MenuModel(rawValue: indexPath.row)
            
            cell.imgView.image = menuModel?.image
            cell.label.text = menuModel?.description
            
            
            return cell
    }
    
    func getNumberOfRows() -> Int {
        return 3
    }
}
