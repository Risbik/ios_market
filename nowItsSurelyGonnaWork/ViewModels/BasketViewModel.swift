//
//  BasketViewModel.swift
//  ios_market
//
//  Created by Dell Smith on 19/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import Foundation

struct Items {
    let item: Item
    var number: UInt32
}

class BasketViewModel {
    static var items: [Items] = []
    
    fileprivate init() {
    }
    
    static func addToBasket(_item: Item, _number: UInt32) {
        var items = BasketViewModel.getItemsInBasket()
        
        for index in 0 ..< items.count {
            if (items[index].item.name == _item.name) {
                items[index].number += _number
                setItems(_items: items)
                return
            }
        }
        
        items.append(Items(item: _item, number: _number))
        setItems(_items: items)
    }
    
    static func getItemsInBasket() -> [Items] {
        return BasketViewModel.items
    }
    
    static fileprivate func setItems(_items: [Items]) -> () {
        BasketViewModel.items = _items
    }
    
    static func removeItem(_item: Item, _number: UInt32) {
        var items = BasketViewModel.getItemsInBasket()
        
        for index in 0 ..< items.count {
            if (items[index].item.name == _item.name) {
                items[index].number -= _number
                if (items[index].number <= 0) { items.remove(at: index)}
                return
            }
        }
    }
    
    static func emptyBasket() {
        BasketViewModel.items = []
    }
    
    static func removeItem(_item: Item) {
        var items = BasketViewModel.getItemsInBasket()
        
        for index in 0 ..< items.count {
            if (items[index].item.name == _item.name) {
                items.remove(at: index)
                BasketViewModel.setItems(_items: items)
                return
                }
        }
    }
    
    static func updateItem(index: Int, newNumber: UInt32) {
        var items = BasketViewModel.getItemsInBasket()
        items[index].number = newNumber
        BasketViewModel.setItems(_items: items)
    }
}
