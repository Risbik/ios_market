//
//  ItemViewModel.swift
//  ios_market
//
//  Created by Гость on 18/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import Foundation
import UIKit


class ItemViewModel {
    var data: [Item] = []
    var sortedByPrice = false
    var sortedByName = false
    
    @IBOutlet var tableView: UITableView!
    
//    func getCell(indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "_cell", for: indexPath) as! DataCell
//
//
//        weak var _data = data[indexPath.row]
//
//        cell.nameView.text = _data!.name
//        cell.costView.text = String(_data!.cost)
//        cell.imgView.image = _data!.img[0]
//        //cell.descView.text = _data!.desc
//        cell.moreButton.tag = indexPath.row
//        cell.layer.cornerRadius = 15
//        cell.layer.masksToBounds = true
//        cell.layer.borderColor = UIColor.darkGray.cgColor
//        cell.layer.borderWidth = 0.8
//        return cell
//
//    }
    
    func getNumberOfCells() -> Int {
        return data.count
    }
    
    
    func sortByPrice() -> [Item] {
        if (sortedByPrice)
        {
            data.sort(by: {$0.cost > $1.cost})
            
        }
        else
        {
            data.sort(by: {$0.cost < $1.cost})
        }
        sortedByPrice = !sortedByPrice
        return data
        
        
    }
    
    func sortByName() -> [Item] {
        if (sortedByName)
        {
            data.sort(by: {$0.name > $1.name})
            
        }
        else
        {
            data.sort(by: {$0.name < $1.name})
        }
        sortedByName = !sortedByName
        return data
      
    }
  
    
    
    

    init() {
        
    }
    
    

}
