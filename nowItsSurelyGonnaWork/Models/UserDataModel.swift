//
//  UserDataModel.swift
//  ios_market
//
//  Created by Vladislav on 10/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import Foundation

class UserDataModel: Codable
{
    var users: [UserKey] = []
    
    init() {
        
    }
}

class UserKey: Codable
{
    var userName: String!
    var userData: UserData!
    enum CodingKeys: String, CodingKey
    {
        case userName = "user_name"
        case userData = "user_data"
    }
    
    init()
    {
        
    }
}

class UserData: Codable
{
    var userPassword: String!
    var userFirstName: String!
    var userLastName: String!
    var userMoney: UInt32!
    
    enum CodingKeys: String, CodingKey
    {
        case userPassword = "user_password"
        case userFirstName = "first_name"
        case userLastName = "last_name"
        case userMoney = "user_money"
    }
    
    init() {
        
    }
}

