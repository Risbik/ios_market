//
//  File.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 28/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import Foundation
import UIKit

enum MenuModel: Int, CustomStringConvertible
{
    case Profile
    case Basket
    case Market
    
    var description: String
    {
        switch self {
        case .Profile:
            return "Profile"
        case .Basket:
            return "Basket"
        case .Market:
            return "Market"
        }
    }
    
    var image: UIImage
    {
        switch self {
        case .Profile:
            return UIImage(named: "Profile") ?? UIImage()
        case .Basket:
            return UIImage(named: "Basket") ?? UIImage()
        case .Market:
            return UIImage(named: "Market") ?? UIImage()
        }
        
    }
    
}
