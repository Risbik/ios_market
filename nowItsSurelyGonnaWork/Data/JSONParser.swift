//
//  JSONParser.swift
//  ios_market
//
//  Created by Vladislav on 10/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import Foundation

class JSONParser
{
    static let dataFolder = "ios_marketData"
    static let dataFile = "/users.json"
    static func decodeUserData() -> UserDataModel?
    {
        let file = JSONParser.getDocumentsDataPath()
        if file.path == nil { return nil }
        if file.exist == false { return nil }
        
        
        let path = (file.path!.path + JSONParser.dataFile)
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let ipString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)!
                let jsonData = ipString.data(using: String.Encoding.utf8.rawValue,allowLossyConversion: true)!
                let json = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)
                
                print(json)
                
                let decoder = JSONDecoder()
                
                
                
                let users = try! decoder.decode(UserDataModel.self, from: data)
                
                //dump(users)
                return users
                
            } catch let error {
                print("failed to convert data \(error)")
                return nil
            }
        return nil
    }
    
    static func saveUserData(users: UserDataModel)
    {
        // 0 - Bool, 1 - URL
        let file = JSONParser.getDocumentsDataPath()
        if file.path == nil { return }
        
        let fileManager = FileManager.default
        if file.exist {
            do {
                let encoder = JSONEncoder()
                let temp = try encoder.encode(users)
                let dat = try JSONSerialization.jsonObject(with: temp, options: JSONSerialization.ReadingOptions.allowFragments)
                let data = try JSONSerialization.data(withJSONObject: dat, options: .prettyPrinted)
                
               try data.write(to: URL(fileURLWithPath: file.path!.path + JSONParser.dataFile))
                print("File exists, data saved")
            } catch let error {
                print(error)
            }
        } else {
            do {
                try fileManager.createDirectory(at: file.path!, withIntermediateDirectories: true, attributes: nil)
                fileManager.createFile(atPath: file.path!.path + JSONParser.dataFile, contents: nil, attributes: nil)
                
                let encoder = JSONEncoder()
                let temp = try encoder.encode(users)
                let dat = try JSONSerialization.jsonObject(with: temp, options: JSONSerialization.ReadingOptions.allowFragments)
                let data = try JSONSerialization.data(withJSONObject: dat, options: .prettyPrinted)
                
                
                
                try data.write(to: URL(fileURLWithPath: file.path!.path + JSONParser.dataFile))
                print("File created, data saved")
            } catch let error {
                print(error)
            }
        }
            
            /*
        
            
            
            if let path = Bundle.main.path(forResource: "users", ofType: "json")
            {
                do {
                    try data.write(to: URL(fileURLWithPath: path), options: [])
                    
                    print("saved")
                } catch let error {
                    print("failed to save data \(error)")
                }
            }
            else
            {
                /*
                let fileManager = FileManager.default
                let newFile = fileManager.appending
                
                
                fileManager.createFile(atPath: path, contents: data, attributes: nil)
                do {
                    try data.write(to: URL(fileURLWithPath: path), options: [])
                    print("can't find file, so creating new and saving")
                }
                catch let error {
                    print("woops, there's an error: \(error)")
                }
                */
            }
            
        }
        catch let error {
            print("woops, there's an error: \(error)")
            return
        }
        
        */
    }
    
    static func getDocumentsDataPath() -> (exist: Bool, path: URL?) {
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let fileURL = documentDirectory.appendingPathComponent(JSONParser.dataFolder + JSONParser.dataFile)
            
            if (fileManager.fileExists(atPath: fileURL.path)) {
                return (true, documentDirectory.appendingPathComponent(JSONParser.dataFolder))
            }
            else {
                return (false, documentDirectory.appendingPathComponent(JSONParser.dataFolder))
            }
            
            }
         catch {
            print(error)
            
        }
        return (false, nil)
    }
    
    
    static func updateUserData(user: UserKey)
    {
        let users = JSONParser.decodeUserData()
        
        for index in 0 ..< users!.users.count {
            if users!.users[index].userName == user.userName {
                users!.users.remove(at: index)
                users!.users.append(user)
                break
            }
        }
        

    }
    
    
}
