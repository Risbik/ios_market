//
//  AdsPrototype.swift
//  ios_market
//
//  Created by Dell Smith on 20/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import Foundation
import UIKit

struct AdsPrototype {
    var desc: String?
    var img: UIImage!
    var oldCost: String?
    var newCost: String?
}
